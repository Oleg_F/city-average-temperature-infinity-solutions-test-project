package com.testproject.citytemperature.dao.repository;

import com.testproject.citytemperature.dao.model.City;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CityRepository extends JpaRepository<City, Long> {
    City findByNameAndCountryId(String name, Long countryId);
}
