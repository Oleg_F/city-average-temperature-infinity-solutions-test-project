package com.testproject.citytemperature.dao.repository;

import com.testproject.citytemperature.dao.model.Country;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CountryRepository extends JpaRepository<Country, Long> {
    Country findByName(String name);
}