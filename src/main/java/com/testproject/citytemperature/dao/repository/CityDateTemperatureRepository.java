package com.testproject.citytemperature.dao.repository;

import com.testproject.citytemperature.dao.model.CityDateTemperature;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.Instant;
import java.util.List;

public interface CityDateTemperatureRepository extends JpaRepository<CityDateTemperature, Long> {
    CityDateTemperature findFirstByCityIdOrderByDateDesc(Long cityId);

    List<CityDateTemperature> findByCityIdAndDateBetweenOrderByDateAsc(Long cityId, Instant start, Instant end);
}
