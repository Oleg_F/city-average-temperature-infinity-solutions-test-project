package com.testproject.citytemperature.service;

import static java.util.Objects.isNull;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.apache.commons.collections4.MapUtils.isEmpty;

import com.testproject.citytemperature.api.dto.CityTemperatureDto;
import com.testproject.citytemperature.dao.model.City;
import com.testproject.citytemperature.dao.model.CityDateTemperature;
import com.testproject.citytemperature.dao.model.Country;
import com.testproject.citytemperature.dao.repository.CityDateTemperatureRepository;
import com.testproject.citytemperature.mapper.LocalDateMapper;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
@RequiredArgsConstructor
@EnableScheduling
@ConfigurationProperties(prefix = "app.city-date-temperature.update")
@Slf4j
public class CityDateTemperatureService {
    @Setter
    @Getter
    private Map<String, Set<String>> citiesByCountry;

    private final WeatherService weatherService;
    private final CountryService countryService;
    private final CityService cityService;
    private final CityDateTemperatureRepository repository;
    private final LocalDateMapper localDateMapper;

    public CityDateTemperature getWithMaxDate(@NonNull Long cityId) {
        return repository.findFirstByCityIdOrderByDateDesc(cityId);
    }

    public List<CityDateTemperature> getListOrderedByDate(@NonNull Long cityId, @NonNull LocalDate date) {
        Instant start = localDateMapper.toInstant(date, true);
        Instant end = localDateMapper.toInstant(date, false);

        return repository.findByCityIdAndDateBetweenOrderByDateAsc(cityId, start, end);
    }

    @Scheduled(
            fixedDelayString = "${app.city-date-temperature.update.delay}",
            timeUnit = SECONDS,
            initialDelay = 20
    )
    private void autoSave() {
        if (isEmpty(citiesByCountry) ||
                citiesByCountry.values().stream()
                        .allMatch(CollectionUtils::isEmpty)) {
            String message = "There are no cities to auto save. Check property app.city-date-temperature.update.";
            log.error(message);
            throw new NullPointerException(message);
        }

        Instant date = Instant.now();
        Map<String, Set<CityTemperatureDto>> cityTemperatureDtoSetByCountryMap = weatherService.getCityTemperatureDtoSetByCountryNameMap(citiesByCountry);
        if (isEmpty(cityTemperatureDtoSetByCountryMap) ||
                cityTemperatureDtoSetByCountryMap.values().stream()
                        .allMatch(CollectionUtils::isEmpty)) {
            String message = String.format("There are no data for cities [%s] to auto save. Skip saving until next iteration.", citiesByCountry);
            log.error(message);
            throw new RuntimeException(message);
        }

        cityTemperatureDtoSetByCountryMap.forEach((countryName, cityTemperatureDtoSet) -> {
            Country country = countryService.saveIfAbsentOrReturn(countryName);
            if (isNull(country)) {
                log.error(String.format("Country with name=% not saved, try to save next country.", countryName));
                return;
            }

            cityTemperatureDtoSet.forEach(cityTemperatureDto -> save(cityTemperatureDto, date, country.getId()));
        });
    }

    private void save(@NonNull CityTemperatureDto cityTemperatureDto, @NonNull Instant date, @NonNull Long countryId) {
        String cityName = cityTemperatureDto.getCityName();
        if (isNull(cityName)) {
            String message = "cityName can't be null";
            log.error(message);
            throw new NullPointerException(message);
        }

        Double temperature = cityTemperatureDto.getTemperature();
        if (isNull(temperature)) {
            String message = "temperature can't be null";
            log.error(message);
            throw new NullPointerException(message);
        }

        City city = cityService.saveIfAbsentOrReturn(cityName, countryId);
        if (isNull(city)) {
            String message = "city can't be null";
            log.error(message);
            throw new NullPointerException(message);
        }

        save(date, temperature, city.getId());
    }

    private CityDateTemperature save(@NonNull Instant date, @NonNull Double temperature, @NonNull Long cityId) {
        return save(
                CityDateTemperature.builder()
                        .date(date)
                        .temperature(temperature)
                        .cityId(cityId)
                        .build()
        );
    }

    private CityDateTemperature save(@NonNull CityDateTemperature model) {
        return repository.save(model);
    }
}