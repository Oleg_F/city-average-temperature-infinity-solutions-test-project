package com.testproject.citytemperature.service;

import com.testproject.citytemperature.api.dto.CityTemperatureDto;

public interface WeatherProviderService {
    CityTemperatureDto getCityTemperatureDto(String countryCode, String cityName);
}