package com.testproject.citytemperature.service;

import static org.springframework.http.HttpStatus.NO_CONTENT;

import com.testproject.citytemperature.api.dto.CityTemperatureDto;
import com.testproject.citytemperature.api.dto.CurrentObsDto;
import com.testproject.citytemperature.api.dto.CurrentObsGroupDto;
import com.testproject.citytemperature.feign.WeatherbitFeinClient;
import com.testproject.citytemperature.mapper.CurrentObsDtoMapper;
import feign.FeignException;
import feign.RetryableException;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;

@Service
@Slf4j
public class WeatherbitService implements WeatherProviderService {
    private final String API_KEY;

    private final WeatherbitFeinClient feinClient;
    private final CurrentObsDtoMapper currentObsDtoMapper;

    @Autowired
    public WeatherbitService(WeatherbitFeinClient feignClient,
                             CurrentObsDtoMapper currentObsDtoMapper,
                             Environment environment) {
        this.API_KEY = environment.getRequiredProperty("app.weather-providers.weatherbit.key");
        this.feinClient = feignClient;
        this.currentObsDtoMapper = currentObsDtoMapper;
    }

    @Override
    public CityTemperatureDto getCityTemperatureDto(@NonNull String countryCode, @NonNull String cityName) {
        ResponseEntity<CurrentObsGroupDto> currentObsGroupDtoResponseEntity;

        try {
            currentObsGroupDtoResponseEntity = feinClient.getCurrentWeather(countryCode, cityName, API_KEY);
            if (currentObsGroupDtoResponseEntity.getStatusCode() == NO_CONTENT) {
                String message = String.format("%s countryCode=%s cityName=%s", NO_CONTENT.getReasonPhrase(), countryCode, cityName);
                log.error(message);
                throw new RuntimeException(message);
            }
        } catch (RetryableException e) {
            log.error("{} countryCode={}, cityName={}", e.getMessage(), countryCode, cityName);
            throw e;
        } catch (FeignException e) {
            byte[] responseBodyByteArray = e.responseBody().get().array();
            String responseBodyString = new String(responseBodyByteArray, StandardCharsets.UTF_8);
            log.error("{} countryCode={} cityName={}", responseBodyString, countryCode, cityName);
            throw e;
        }

        CurrentObsDto currentObsDto = currentObsGroupDtoResponseEntity.getBody().getData().get(0);

        return currentObsDtoMapper.toCityTemperatureDto(currentObsDto);
    }
}