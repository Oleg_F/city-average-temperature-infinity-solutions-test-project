package com.testproject.citytemperature.service;

import static java.util.Objects.isNull;
import static org.apache.commons.collections4.CollectionUtils.isEmpty;

import com.testproject.citytemperature.api.dto.DateTemperatureDto;
import com.testproject.citytemperature.dao.model.City;
import com.testproject.citytemperature.dao.model.CityDateTemperature;
import com.testproject.citytemperature.mapper.CityDateTemperatureMapper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DateTemperatureService {
    private final CityService cityService;
    private final CityDateTemperatureService cityDateTemperatureService;

    private final CityDateTemperatureMapper cityDateTemperatureMapper;

    public DateTemperatureDto getWithMaxDate(@NonNull String countryName, @NonNull String cityName) {
        City city = cityService.get(cityName, countryName);
        if (isNull(city)) {
            return null;
        }

        Long cityId = city.getId();
        if (isNull(cityId)) {
            return null;
        }

        CityDateTemperature cityDateTemperature = cityDateTemperatureService.getWithMaxDate(cityId);
        if (isNull(cityDateTemperature)) {
            return null;
        }

        return cityDateTemperatureMapper.toDateTemperatureDto(cityDateTemperature);
    }

    public List<DateTemperatureDto> getList(@NonNull String countryName, @NonNull String cityName, @NonNull LocalDate date) {
        City city = cityService.get(cityName, countryName);
        if (isNull(city)) {
            return null;
        }

        Long cityId = city.getId();
        if (isNull(cityId)) {
            return null;
        }

        List<CityDateTemperature> cityDateTemperatureList = cityDateTemperatureService.getListOrderedByDate(cityId, date);
        if (isEmpty(cityDateTemperatureList)) {
            return null;
        }

        return cityDateTemperatureMapper.iterableToDateTemperatureDtoList(cityDateTemperatureList);
    }
}