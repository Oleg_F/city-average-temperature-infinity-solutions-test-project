package com.testproject.citytemperature.service;

import com.testproject.citytemperature.api.dto.ApiNinjaWeatherResponseDto;
import com.testproject.citytemperature.api.dto.CityTemperatureDto;
import com.testproject.citytemperature.feign.ApiNinjasFeignClient;
import com.testproject.citytemperature.mapper.ApiNinjaWeatherResponseDtoMapper;
import feign.FeignException;
import feign.RetryableException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldNameConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;

@Service
@FieldNameConstants
@RequiredArgsConstructor
@Slf4j
public class ApiNinjaService implements WeatherProviderService {
    private final String API_KEY;

    private final ApiNinjasFeignClient feignClient;
    private final ApiNinjaWeatherResponseDtoMapper apiNinjaWeatherResponseDtoMapper;

    @Autowired
    public ApiNinjaService(ApiNinjasFeignClient feignClient,
                           ApiNinjaWeatherResponseDtoMapper apiNinjaWeatherResponseDtoMapper,
                           Environment environment) {
        this.API_KEY = environment.getRequiredProperty("app.weather-providers.api-ninja.key");
        this.feignClient = feignClient;
        this.apiNinjaWeatherResponseDtoMapper = apiNinjaWeatherResponseDtoMapper;
    }

    @Override
    public CityTemperatureDto getCityTemperatureDto(@NonNull String countryCode, @NonNull String cityName) {
        ApiNinjaWeatherResponseDto weatherResponseDto;

        try {
            weatherResponseDto = feignClient.getCurrentWeather(cityName, countryCode, API_KEY);
        } catch (RetryableException e) {
            log.error("{} countryCode={}, cityName={}", e.getMessage(), countryCode, cityName);
            throw e;
        } catch (FeignException e) {
            byte[] responseBodyByteArray = e.responseBody().get().array();
            String responseBodyString = new String(responseBodyByteArray, StandardCharsets.UTF_8);
            log.error("{} countryCode={}, cityName={}", responseBodyString, countryCode, cityName);
            throw e;
        }

        return apiNinjaWeatherResponseDtoMapper.toCityTemperatureDto(weatherResponseDto);
    }
}