package com.testproject.citytemperature.service;

import static java.util.Objects.isNull;

import com.testproject.citytemperature.dao.model.Country;
import com.testproject.citytemperature.dao.repository.CountryRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CountryService {
    public static final Map<String, String> codeByNameMap = getCodeByNameMap();

    private final CountryRepository repository;

    private static Map<String, String> getCodeByNameMap() {
        return Arrays.stream(Locale.getISOCountries())
                .map(isoCountry -> new Locale("", isoCountry))
                .collect(Collectors.toMap(Locale::getDisplayCountry, Locale::getCountry));
    }

    public Country get(@NonNull String name) {
        return repository.findByName(name);
    }

    public String getCode(@NonNull String name) {
        return codeByNameMap.get(name);
    }

    public Country saveIfAbsentOrReturn(@NonNull String name) {
        Country model = get(name);

        if (isNull(model)) {
            model = save(
                    Country.builder()
                            .name(name)
                            .build()
            );
        }

        return model;
    }

    private Country save(@NonNull Country model) {
        return repository.save(model);
    }
}