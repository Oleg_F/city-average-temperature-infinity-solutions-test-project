package com.testproject.citytemperature.service;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static org.apache.commons.collections4.MapUtils.isEmpty;

import com.testproject.citytemperature.api.dto.CityTemperatureDto;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldNameConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

@Service
@RequiredArgsConstructor
@Slf4j
@FieldNameConstants
public class WeatherService {
    private final CountryService countryService;
    private final List<WeatherProviderService> weatherProviderServiceList;

    public Map<String, Set<CityTemperatureDto>> getCityTemperatureDtoSetByCountryNameMap(Map<String, Set<String>> cityNameSetByCountryNameMap) {
        if (isEmpty(cityNameSetByCountryNameMap)) {
            String message = "cityNameSetByCountryNameMap has no data to work with";
            log.error(message);
            throw new RuntimeException(message);
        }

        Map<String, Set<CityTemperatureDto>> cityTemperatureDtoSetByCountryMap = new HashMap<>();

        cityNameSetByCountryNameMap.forEach((countryName, cityNameSet) -> {
            String countryCode = countryService.getCode(countryName);
            if (isNull(countryCode)) {
                log.info("There is no country with name = %s", countryName);
                return;
            }

            cityNameSet.forEach(cityName -> {
                CityTemperatureDto cityTemperatureDto = getAverageCityTemperatureDto(countryCode, cityName);
                if (isNull(cityTemperatureDto)) {
                    return;
                }

                Set<CityTemperatureDto> cityTemperatureDtoSet = cityTemperatureDtoSetByCountryMap.get(countryName);
                if (isNull(cityTemperatureDtoSet)) {
                    cityTemperatureDtoSetByCountryMap.put(countryName, new HashSet<>(Arrays.asList(cityTemperatureDto)));
                } else {
                    cityTemperatureDtoSet.add(cityTemperatureDto);
                }
            });
        });

        return cityTemperatureDtoSetByCountryMap;
    }

    private CityTemperatureDto getAverageCityTemperatureDto(@NonNull String countryCode, @NonNull String cityName) {
        CityTemperatureDto result = new CityTemperatureDto();
        AtomicInteger counter = new AtomicInteger();

        weatherProviderServiceList.stream()
                .map(weatherProviderService -> weatherProviderService.getCityTemperatureDto(countryCode, cityName))
                .filter(cityTemperatureDto -> nonNull(cityTemperatureDto) && nonNull(cityTemperatureDto.getTemperature()))
                .forEach(cityTemperatureDto -> {
                    Double temperature = cityTemperatureDto.getTemperature();

                    Double resultTemperature = result.getTemperature();
                    if (isNull(resultTemperature)) {
                        resultTemperature = temperature;
                    } else {
                        resultTemperature = resultTemperature + temperature;
                    }

                    result.setTemperature(resultTemperature);
                    counter.incrementAndGet();
                });

        Double resultTemperature = result.getTemperature();

        return isNull(resultTemperature) ?
                null :
                result
                        .setCityName(cityName)
                        .setTemperature(resultTemperature / counter.get());
    }
}