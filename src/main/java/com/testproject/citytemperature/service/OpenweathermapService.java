package com.testproject.citytemperature.service;

import com.testproject.citytemperature.api.dto.CityTemperatureDto;
import com.testproject.citytemperature.api.dto.OpenweathermapWeatherResponseDto;
import com.testproject.citytemperature.feign.OpenweathermapFeignClient;
import com.testproject.citytemperature.mapper.OpenweathermapWeatherResponseDtoMapper;
import feign.FeignException;
import feign.RetryableException;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;

@Service
@Slf4j
public class OpenweathermapService implements WeatherProviderService {
    private final String API_KEY;
    private final OpenweathermapFeignClient feignClient;
    private final OpenweathermapWeatherResponseDtoMapper openweathermapWeatherResponseDtoMapper;

    @Autowired
    public OpenweathermapService(OpenweathermapFeignClient feignClient,
                                 OpenweathermapWeatherResponseDtoMapper openweathermapWeatherResponseDtoMapper,
                                 Environment environment) {
        this.API_KEY = environment.getRequiredProperty("app.weather-providers.openweathermap.key");
        this.feignClient = feignClient;
        this.openweathermapWeatherResponseDtoMapper = openweathermapWeatherResponseDtoMapper;
    }

    @Override
    public CityTemperatureDto getCityTemperatureDto(@NonNull String countryCode, @NonNull String cityName) {
        OpenweathermapWeatherResponseDto weatherResponseDto;

        try {
            weatherResponseDto = feignClient.getCurrentWeather(
                    String.join(",", cityName, countryCode),
                    API_KEY,
                    "metric"
            );
        } catch (RetryableException e) {
            log.error("{} countryCode={}, cityName={}", e.getMessage(), countryCode, cityName);
            throw e;
        } catch (FeignException e) {
            byte[] responseBodyByteArray = e.responseBody().get().array();
            String responseBodyString = new String(responseBodyByteArray, StandardCharsets.UTF_8);
            log.error("{} countryCode={}, cityName={}", responseBodyString, countryCode, cityName);
            throw e;
        }

        return openweathermapWeatherResponseDtoMapper.toCityTemperatureDto(weatherResponseDto);
    }
}
