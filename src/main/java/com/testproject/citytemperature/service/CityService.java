package com.testproject.citytemperature.service;

import static java.util.Objects.isNull;

import com.testproject.citytemperature.dao.model.City;
import com.testproject.citytemperature.dao.model.Country;
import com.testproject.citytemperature.dao.repository.CityRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CityService {
    private final CityRepository repository;
    private final CountryService countryService;

    public City get(@NonNull String name, @NonNull String countryName) {
        Country country = countryService.get(countryName);
        if (isNull(country)) {
            return null;
        }

        return get(name, country.getId());
    }

    public City saveIfAbsentOrReturn(@NonNull String name, @NonNull Long countryId) {
        City city = get(name, countryId);
        if (isNull(city)) {
            city = save(
                    City.builder()
                            .name(name)
                            .countryId(countryId)
                            .build()
            );
        }

        return city;
    }

    private City save(@NonNull City model) {
        return repository.save(model);
    }

    private City get(@NonNull String name, @NonNull Long countryId) {
        return repository.findByNameAndCountryId(name, countryId);
    }
}