package com.testproject.citytemperature.feign;

import com.testproject.citytemperature.api.dto.ApiNinjaWeatherResponseDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(
        name = "api-ninjas",
        url = "https://api.api-ninjas.com/v1/weather"
)
public interface ApiNinjasFeignClient {
    @GetMapping
    ApiNinjaWeatherResponseDto getCurrentWeather(@RequestParam("city") String cityName,
                                                 @RequestParam("country") String countryCode,
                                                 @RequestHeader("X-Api-Key") String key);
}