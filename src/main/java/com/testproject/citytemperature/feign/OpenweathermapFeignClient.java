package com.testproject.citytemperature.feign;

import com.testproject.citytemperature.api.dto.OpenweathermapWeatherResponseDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(
        name = "openweathermap",
        url = "https://api.openweathermap.org/data/2.5/weather"
)
public interface OpenweathermapFeignClient {
    @GetMapping
    OpenweathermapWeatherResponseDto getCurrentWeather(@RequestParam("q") String cityNameCommaCountryCode,
                                                       @RequestParam("appid") String key,
                                                       @RequestParam(name = "units", required = false) String units);
}