package com.testproject.citytemperature.feign;

import com.testproject.citytemperature.api.dto.CurrentObsGroupDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(
        name = "weatherbit",
        url = "https://api.weatherbit.io/v2.0/current"
)
public interface WeatherbitFeinClient {
    @GetMapping
    ResponseEntity<CurrentObsGroupDto> getCurrentWeather(@RequestParam("country") String countryCode,
                                                        @RequestParam("city") String cityName,
                                                        @RequestParam("key") String key);
}