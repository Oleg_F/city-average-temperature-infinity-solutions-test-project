package com.testproject.citytemperature.configuration;

import feign.Feign;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;

@EnableFeignClients(
        basePackages = "com.testproject.citytemperature.feign",
        defaultConfiguration = FeignConfiguration.class
)
@Configuration
@ConditionalOnClass(Feign.class)
public class FeignConfiguration {
}