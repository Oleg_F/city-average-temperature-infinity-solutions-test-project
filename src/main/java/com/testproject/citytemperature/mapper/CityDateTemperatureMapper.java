package com.testproject.citytemperature.mapper;

import com.testproject.citytemperature.api.dto.DateTemperatureDto;
import com.testproject.citytemperature.dao.model.CityDateTemperature;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface CityDateTemperatureMapper {
    DateTemperatureDto toDateTemperatureDto(CityDateTemperature model);

    List<DateTemperatureDto> iterableToDateTemperatureDtoList(Iterable<CityDateTemperature> iterable);
}
