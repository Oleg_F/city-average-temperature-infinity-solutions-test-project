package com.testproject.citytemperature.mapper;

import lombok.NonNull;
import org.mapstruct.Mapper;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;

@Mapper
public interface LocalDateMapper {
    default Instant toInstant(@NonNull LocalDate model, boolean atStartDay) {
        LocalDateTime localDateTime = atStartDay ?
                model.atStartOfDay() : LocalDateTime.of(model, LocalTime.MAX);
        return localDateTime.atZone(ZoneId.systemDefault()).toInstant();
    }
}