package com.testproject.citytemperature.mapper;

import com.testproject.citytemperature.api.dto.CityTemperatureDto;
import com.testproject.citytemperature.api.dto.CurrentObsDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface CurrentObsDtoMapper {
    @Mapping(target = "temperature", source = "temp")
    CityTemperatureDto toCityTemperatureDto(CurrentObsDto model);
}