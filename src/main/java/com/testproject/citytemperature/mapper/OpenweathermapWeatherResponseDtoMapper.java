package com.testproject.citytemperature.mapper;

import com.testproject.citytemperature.api.dto.CityTemperatureDto;
import com.testproject.citytemperature.api.dto.OpenweathermapWeatherResponseDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface OpenweathermapWeatherResponseDtoMapper {
    @Mapping(target = "cityName", source = "name")
    @Mapping(target = "temperature", source = "main.temp")
    CityTemperatureDto toCityTemperatureDto(OpenweathermapWeatherResponseDto model);
}
