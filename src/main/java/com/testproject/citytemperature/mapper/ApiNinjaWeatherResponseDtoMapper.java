package com.testproject.citytemperature.mapper;

import com.testproject.citytemperature.api.dto.ApiNinjaWeatherResponseDto;
import com.testproject.citytemperature.api.dto.CityTemperatureDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface ApiNinjaWeatherResponseDtoMapper {
    @Mapping(target = "temperature", source = "temp")
    CityTemperatureDto toCityTemperatureDto(ApiNinjaWeatherResponseDto model);
}