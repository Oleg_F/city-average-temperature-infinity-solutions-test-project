package com.testproject.citytemperature.api.dto;

//    city_name (string, optional): City name (closest) ,
//    state_code (string, optional): State abbreviation ,
//    country_code (string, optional): Country abbreviation ,
//    timezone (string, optional): Local IANA time zone ,
//    lat (number, optional): Latitude ,
//    lon (number, optional): Longitude ,
//    station (string, optional): Source Station ID ,
//    sources (Array[string], optional): List of data sources used in response ,
//    vis (integer, optional): Visibility - default (M) ,
//    rh (integer, optional): Relative humidity (%) ,
//    dewpt (number, optional): Dew point temperature - default (C) ,
//    wind_dir (integer, optional): Wind direction (degrees) ,
//    wind_cdir (string, optional): Cardinal wind direction ,
//    wind_cdir_full (string, optional): Cardinal wind direction (text) ,
//    wind_speed (number, optional): Wind speed - Default (m/s) ,
//    temp (number, optional): Temperature - Default (C) ,
//    app_temp (number, optional): Apparent temperature - Default (C) ,
//    clouds (integer, optional): Cloud cover (%) ,
//    weather (inline_model_3, optional),
//    datetime (string, optional): Cycle Hour (UTC) of observation ,
//    ob_time (string, optional): Full time (UTC) of observation (YYYY-MM-DD HH:MM) ,
//    ts (number, optional): Unix Timestamp ,
//    sunrise (string, optional): Time (UTC) of Sunrise (HH:MM) ,
//    sunset (string, optional): Time (UTC) of Sunset (HH:MM) ,
//    slp (number, optional): Mean sea level pressure in millibars (mb) ,
//    pres (number, optional): Pressure (mb) ,
//    aqi (number, optional): Air quality index (US EPA standard 0 to +500) ,
//    uv (number, optional): UV Index ,
//    solar_rad (number, optional): Estimated solar radiation (W/m^2) ,
//    ghi (number, optional): Global horizontal irradiance (W/m^2) ,
//    dni (number, optional): Direct normal irradiance (W/m^2) ,
//    dhi (number, optional): Diffuse horizontal irradiance (W/m^2) ,
//    elev_angle (number, optional): Current solar elevation angle (Degrees) ,
//    hour_angle (number, optional): Current solar hour angle (Degrees) ,
//    pod (string, optional): Part of the day (d = day, n = night) ,
//    precip (number, optional): Precipitation in last hour - Default (mm) ,
//    snow (number, optional): Snowfall in last hour - Default (mm)

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CurrentObsDto {
    private String cityName;
    private Double temp;
}