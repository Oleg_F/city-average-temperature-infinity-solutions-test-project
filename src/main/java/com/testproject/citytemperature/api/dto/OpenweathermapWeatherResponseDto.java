package com.testproject.citytemperature.api.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

//{
//        "coord": {
//        "lon": 37.6156,
//        "lat": 55.7522
//        },
//        "weather": [
//        {
//        "id": 804,
//        "main": "Clouds",
//        "description": "overcast clouds",
//        "icon": "04n"
//        }
//        ],
//        "base": "stations",
//        "main": {
//        "temp": 22.07,
//        "feels_like": 21.35,
//        "temp_min": 18.1,
//        "temp_max": 23.29,
//        "pressure": 1020,
//        "humidity": 39,
//        "sea_level": 1020,
//        "grnd_level": 1003
//        },
//        "visibility": 10000,
//        "wind": {
//        "speed": 2.19,
//        "deg": 94,
//        "gust": 5.29
//        },
//        "clouds": {
//        "all": 100
//        },
//        "dt": 1661635177,
//        "sys": {
//        "type": 2,
//        "id": 2018597,
//        "country": "RU",
//        "sunrise": 1661653577,
//        "sunset": 1661704553
//        },
//        "timezone": 10800,
//        "id": 524901,
//        "name": "Moscow",
//        "cod": 200
//        }

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OpenweathermapWeatherResponseDto {
    //CityName
    private String name;
    private OpenweathermapWeatherMainResponseDto main;
}
