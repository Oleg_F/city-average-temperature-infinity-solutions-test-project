package com.testproject.citytemperature.api.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

//response
//{
//        "cloud_pct":100,
//        "temp":19,
//        "feels_like":19,
//        "humidity":68,
//        "min_temp":16,
//        "max_temp":21,
//        "wind_speed":3.09,
//        "wind_degrees":50,
//        "sunrise":1661490207,
//        "sunset":1661540503
//        }

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApiNinjaWeatherResponseDto {
    private Double temp;
}