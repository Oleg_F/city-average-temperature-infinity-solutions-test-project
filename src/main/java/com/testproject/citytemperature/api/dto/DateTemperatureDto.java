package com.testproject.citytemperature.api.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;

@Getter
@Setter
@Builder
public class DateTemperatureDto {
    private Instant date;
    private Double temperature;
}