package com.testproject.citytemperature.api.resource;

import static java.util.Objects.isNull;
import static org.apache.commons.collections4.CollectionUtils.isEmpty;
import static org.springframework.http.ResponseEntity.ok;

import com.testproject.citytemperature.api.dto.DateTemperatureDto;
import com.testproject.citytemperature.service.DateTemperatureService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.text.WordUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/weather/countries/{countryName}/cities/{cityName}")
public class CountryCityTemperatureApiResource {
    private final DateTemperatureService dateTemperatureService;

    @GetMapping
    public ResponseEntity<List<DateTemperatureDto>> get(@PathVariable("countryName") String countryName,
                                                        @PathVariable("cityName") String cityName,
                                                        @RequestParam(name = "date", required = false) LocalDate date) {
        countryName = WordUtils.capitalize(countryName);
        cityName = WordUtils.capitalize(cityName);
        if (isNull(date)) {
            DateTemperatureDto dateTemperatureDtoWithMaxDate = dateTemperatureService.getWithMaxDate(countryName, cityName);
            if (isNull(dateTemperatureDtoWithMaxDate)) {
                return ResponseEntity.noContent().build();
            }

            return ok(Collections.singletonList(dateTemperatureDtoWithMaxDate));
        }

        List<DateTemperatureDto> dateTemperatureDtoList = dateTemperatureService.getList(countryName, cityName, date);
        if (isEmpty(dateTemperatureDtoList)) {
            return ResponseEntity.noContent().build();
        }

        return ok(dateTemperatureDtoList);
    }
}