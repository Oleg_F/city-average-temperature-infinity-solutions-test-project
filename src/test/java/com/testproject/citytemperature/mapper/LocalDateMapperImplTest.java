package com.testproject.citytemperature.mapper;


import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;

@RunWith(SpringJUnit4ClassRunner.class)
public class LocalDateMapperImplTest {
    private final LocalDate LOCAL_DATE = LocalDate.MIN;
//    private boolean atStartDay = true;


    @InjectMocks
    private LocalDateMapperImpl mapper;

    @Test
    public void toInstant_atStartDay() {
        LocalDateTime localDateTime = LOCAL_DATE.atStartOfDay();
        Instant instantExpected = localDateTime.atZone(ZoneId.systemDefault()).toInstant();

        Instant instant = mapper.toInstant(LOCAL_DATE, true);

        assertEquals(instantExpected, instant);
    }

    @Test
    public void toInstant_atEndDay() {
        LocalDateTime localDateTime = LocalDateTime.of(LOCAL_DATE, LocalTime.MAX);
        Instant instantExpected = localDateTime.atZone(ZoneId.systemDefault()).toInstant();

        Instant instant = mapper.toInstant(LOCAL_DATE, false);

        assertEquals(instantExpected, instant);
    }
}