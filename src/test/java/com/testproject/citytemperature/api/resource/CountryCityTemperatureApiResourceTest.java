package com.testproject.citytemperature.api.resource;

import static java.util.Collections.emptyList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;

import com.testproject.citytemperature.api.dto.DateTemperatureDto;
import com.testproject.citytemperature.service.DateTemperatureService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.Instant;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
public class CountryCityTemperatureApiResourceTest {
    private final String COUNTRY_NAME = "Russia";
    private final String CITY_NAME = "Moscow";
    private final LocalDate DATE_LOCAL_DATE = LocalDate.MIN;
    private final Instant DATE_INSTANT = Instant.MIN;
    private final Double TEMPERATURE = Double.valueOf(0);
    private final DateTemperatureDto dateTemperatureDto = DateTemperatureDto.builder()
            .date(DATE_INSTANT)
            .temperature(TEMPERATURE)
            .build();
    private final List<DateTemperatureDto> dateTemperatureDtoList = Collections.singletonList(dateTemperatureDto);

    @Mock
    private DateTemperatureService dateTemperatureService;

    @InjectMocks
    private CountryCityTemperatureApiResource resource;

    @Test
    public void get_ok() {
        when(dateTemperatureService.getList(COUNTRY_NAME, CITY_NAME, DATE_LOCAL_DATE)).thenReturn(dateTemperatureDtoList);

        ResponseEntity<List<DateTemperatureDto>> response = resource.get(COUNTRY_NAME, CITY_NAME, DATE_LOCAL_DATE);

        assertEquals(OK, response.getStatusCode());
        assertEquals(dateTemperatureDtoList, response.getBody());
    }

    @Test
    public void get_dateIsNull() {
        when(dateTemperatureService.getWithMaxDate(COUNTRY_NAME, CITY_NAME)).thenReturn(dateTemperatureDto);

        ResponseEntity<List<DateTemperatureDto>> response = resource.get(COUNTRY_NAME, CITY_NAME, null);

        assertEquals(OK, response.getStatusCode());
        assertEquals(dateTemperatureDtoList, response.getBody());
    }

    @Test
    public void get_dateIsNull_dateTemperatureDtoWithMaxDateIsNull() {
        when(dateTemperatureService.getWithMaxDate(COUNTRY_NAME, CITY_NAME)).thenReturn(null);

        ResponseEntity<List<DateTemperatureDto>> response = resource.get(COUNTRY_NAME, CITY_NAME, null);

        assertEquals(NO_CONTENT, response.getStatusCode());
    }

    @Test
    public void get_dateTemperatureDtoListIsNull() {
        when(dateTemperatureService.getList(COUNTRY_NAME, CITY_NAME, DATE_LOCAL_DATE)).thenReturn(null);

        ResponseEntity<List<DateTemperatureDto>> response = resource.get(COUNTRY_NAME, CITY_NAME, DATE_LOCAL_DATE);

        assertEquals(NO_CONTENT, response.getStatusCode());
    }

    @Test
    public void get_dateTemperatureDtoListIsEmpty() {
        when(dateTemperatureService.getList(COUNTRY_NAME, CITY_NAME, DATE_LOCAL_DATE)).thenReturn(emptyList());

        ResponseEntity<List<DateTemperatureDto>> response = resource.get(COUNTRY_NAME, CITY_NAME, DATE_LOCAL_DATE);

        assertEquals(NO_CONTENT, response.getStatusCode());
    }
}