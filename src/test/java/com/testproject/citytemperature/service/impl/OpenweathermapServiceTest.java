package com.testproject.citytemperature.service.impl;

import static com.testproject.citytemperature.service.impl.OpenweathermapServiceTest.API_KEY;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.Series.CLIENT_ERROR;

import com.testproject.citytemperature.api.dto.CityTemperatureDto;
import com.testproject.citytemperature.api.dto.OpenweathermapWeatherMainResponseDto;
import com.testproject.citytemperature.api.dto.OpenweathermapWeatherResponseDto;
import com.testproject.citytemperature.feign.OpenweathermapFeignClient;
import com.testproject.citytemperature.mapper.OpenweathermapWeatherResponseDtoMapper;
import com.testproject.citytemperature.service.OpenweathermapService;
import com.testproject.citytemperature.utils.FeignExceptionUtils;
import feign.FeignException;
import feign.RetryableException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = OpenweathermapService.class)
@TestPropertySource(properties = "app.weather-providers.openweathermap.key=" + API_KEY)
public class OpenweathermapServiceTest {
    public static final String API_KEY = "eb0baeef5d7057a8210366f923a833f3";
    public final Double TEMP = Double.MIN_VALUE;
    public final Double TEMPERATURE = TEMP;
    private final String API_KEY_BAD = API_KEY;
    private final String CITY_NAME = "Moscow";
    private final String CITY_NAME_BAD = "CityNameBad";
    private final String COUNTRY_CODE = "RU";
    private final String COUNTRY_CODE_BAD = "CountryCodeBad";
    private final String UNITS = "metric";
    private final CityTemperatureDto cityTemperatureDto = CityTemperatureDto.builder()
            .temperature(TEMPERATURE)
            .cityName(CITY_NAME)
            .build();
    private final OpenweathermapWeatherMainResponseDto openweathermapWeatherMainResponseDto = OpenweathermapWeatherMainResponseDto.builder()
            .temp(TEMP)
            .build();
    private final OpenweathermapWeatherResponseDto openweathermapWeatherResponseDto = OpenweathermapWeatherResponseDto.builder()
            .name(CITY_NAME)
            .main(openweathermapWeatherMainResponseDto)
            .build();

    @Autowired
    private OpenweathermapService service;
    @MockBean
    private OpenweathermapFeignClient feignClient;
    @MockBean
    private OpenweathermapWeatherResponseDtoMapper openweathermapWeatherResponseDtoMapper;

    @Before
    public void before() {
        when(openweathermapWeatherResponseDtoMapper.toCityTemperatureDto(openweathermapWeatherResponseDto))
                .thenReturn(cityTemperatureDto);
    }

    @Test
    public void getCityTemperatureDto_ok() {
        when(feignClient.getCurrentWeather(String.join(",", CITY_NAME, COUNTRY_CODE), API_KEY, UNITS))
                .thenReturn(openweathermapWeatherResponseDto);

        CityTemperatureDto cityTemperatureDto = service.getCityTemperatureDto(COUNTRY_CODE, CITY_NAME);

        assertEquals(this.cityTemperatureDto, cityTemperatureDto);
    }

    @Test
    public void getCityTemperatureDto_badCountryCode() {
        when(feignClient.getCurrentWeather(String.join(",", CITY_NAME, COUNTRY_CODE_BAD), API_KEY, UNITS))
                .thenReturn(openweathermapWeatherResponseDto);

        CityTemperatureDto cityTemperatureDto = service.getCityTemperatureDto(COUNTRY_CODE_BAD, CITY_NAME);

        assertEquals(this.cityTemperatureDto, cityTemperatureDto);
    }

    @Test
    public void getCityTemperatureDto_badCityName() {
        String responseBody = "{\"cod\":\"404\",\"message\":\"city not found\"}";
        FeignException feignException = FeignExceptionUtils.create(responseBody, NOT_FOUND.value());

        when(feignClient.getCurrentWeather(String.join(",", CITY_NAME_BAD, COUNTRY_CODE), API_KEY, UNITS))
                .thenThrow(feignException);

        try {
            service.getCityTemperatureDto(COUNTRY_CODE, CITY_NAME_BAD);
        } catch (FeignException e) {
            assertEquals(NOT_FOUND.value(), e.status());
            assertEquals(responseBody, e.contentUTF8());
        }
    }

    @Test
    public void getCityTemperatureDto_badApiKey() {
        String responseBody = "{\"cod\":401, \"message\": \"Invalid API key. Please see http://openweathermap.org/faq#error401 for more info.\"}";
        FeignException feignException = FeignExceptionUtils.create(responseBody, CLIENT_ERROR.value());

        when(feignClient.getCurrentWeather(String.join(",", CITY_NAME, COUNTRY_CODE), API_KEY_BAD, UNITS))
                .thenThrow(feignException);

        try {
            service.getCityTemperatureDto(COUNTRY_CODE, CITY_NAME_BAD);
        } catch (FeignException e) {
            assertEquals(CLIENT_ERROR.value(), e.status());
            assertEquals(responseBody, e.contentUTF8());
        }
    }

    @Test(expected = RetryableException.class)
    public void getCityTemperatureDto_resourceIsUnavailable() {
        when(feignClient.getCurrentWeather(String.join(",", CITY_NAME, COUNTRY_CODE), API_KEY, UNITS))
                .thenThrow(RetryableException.class);

        service.getCityTemperatureDto(COUNTRY_CODE, CITY_NAME);
    }
}