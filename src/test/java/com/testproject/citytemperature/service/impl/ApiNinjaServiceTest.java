package com.testproject.citytemperature.service.impl;

import static com.testproject.citytemperature.service.impl.ApiNinjaServiceTest.API_KEY;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.testproject.citytemperature.api.dto.ApiNinjaWeatherResponseDto;
import com.testproject.citytemperature.api.dto.CityTemperatureDto;
import com.testproject.citytemperature.feign.ApiNinjasFeignClient;
import com.testproject.citytemperature.mapper.ApiNinjaWeatherResponseDtoMapper;
import com.testproject.citytemperature.service.ApiNinjaService;
import com.testproject.citytemperature.service.WeatherProviderService;
import com.testproject.citytemperature.utils.FeignExceptionUtils;
import feign.FeignException;
import feign.RetryableException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ApiNinjaService.class)
@TestPropertySource(properties = "app.weather-providers.api-ninja.key=" + API_KEY)
public class ApiNinjaServiceTest {
    public static final String API_KEY = "njlgowxVJeWszBziNYhzyA==Ib4VZywIn86G29OA";
    public final Double TEMP = Double.MIN_VALUE;
    public final Double TEMPERATURE = TEMP;
    private final String API_KEY_BAD = API_KEY;
    private final String CITY_NAME = "Moscow";
    private final String CITY_NAME_BAD = "CityNameBad";
    private final String COUNTRY_CODE = "RU";
    private final String COUNTRY_CODE_BAD = "CountryCodeBad";
    public final ApiNinjaWeatherResponseDto apiNinjaWeatherResponseDto = ApiNinjaWeatherResponseDto.builder()
            .temp(TEMP)
            .build();
    private final CityTemperatureDto cityTemperatureDto = CityTemperatureDto.builder()
            .cityName(CITY_NAME)
            .temperature(TEMPERATURE)
            .build();

    @MockBean
    private ApiNinjasFeignClient feignClient;
    @MockBean
    private ApiNinjaWeatherResponseDtoMapper apiNinjaWeatherResponseDtoMapper;
    @Autowired
    private WeatherProviderService service;

    @Before
    public void before() {
        when(apiNinjaWeatherResponseDtoMapper.toCityTemperatureDto(any(ApiNinjaWeatherResponseDto.class))).thenReturn(cityTemperatureDto);
    }

    @Test
    public void getCityTemperatureDto_ok() {
        when(feignClient.getCurrentWeather(CITY_NAME, COUNTRY_CODE, API_KEY)).thenReturn(apiNinjaWeatherResponseDto);
        CityTemperatureDto cityTemperatureDto = service.getCityTemperatureDto(COUNTRY_CODE, CITY_NAME);
        assertEquals(this.cityTemperatureDto, cityTemperatureDto);
    }

    @Test
    public void getCityTemperatureDto_countryCodeBad() {
        when(feignClient.getCurrentWeather(CITY_NAME, COUNTRY_CODE_BAD, API_KEY)).thenReturn(apiNinjaWeatherResponseDto);
        CityTemperatureDto cityTemperatureDto = service.getCityTemperatureDto(COUNTRY_CODE_BAD, CITY_NAME);
        assertEquals(this.cityTemperatureDto, cityTemperatureDto);
    }

    @Test
    public void getCityTemperatureDto_apiKeyBad() {
        String responseBody = "{\"error\": \"Invalid API Key.\"}";
        FeignException feignException = FeignExceptionUtils.create(responseBody, HttpStatus.BAD_REQUEST.value());

        when(feignClient.getCurrentWeather(CITY_NAME, COUNTRY_CODE, API_KEY_BAD)).thenThrow(feignException);

        try {
            service.getCityTemperatureDto(COUNTRY_CODE, CITY_NAME);
        } catch (FeignException e) {
            assertEquals(HttpStatus.BAD_REQUEST.value(), e.status());
            assertEquals(responseBody, e.contentUTF8());
        }
    }

    @Test
    public void getCityTemperatureDto_cityNameBad() {
        String responseBody = "{\"error\": \"An unexpected error occured.\"}";
        FeignException feignException = FeignExceptionUtils.create(responseBody, HttpStatus.BAD_REQUEST.value());

        when(feignClient.getCurrentWeather(CITY_NAME_BAD, COUNTRY_CODE, API_KEY)).thenThrow(feignException);

        try {
            service.getCityTemperatureDto(COUNTRY_CODE, CITY_NAME_BAD);
        } catch (FeignException e) {
            assertEquals(HttpStatus.BAD_REQUEST.value(), e.status());
            assertEquals(responseBody, e.contentUTF8());
        }
    }

    @Test(expected = RetryableException.class)
    public void getCityTemperatureDto_resourceIsUnavailable() {
        when(feignClient.getCurrentWeather(CITY_NAME, COUNTRY_CODE, API_KEY)).thenThrow(RetryableException.class);

        service.getCityTemperatureDto(COUNTRY_CODE, CITY_NAME);
    }
}