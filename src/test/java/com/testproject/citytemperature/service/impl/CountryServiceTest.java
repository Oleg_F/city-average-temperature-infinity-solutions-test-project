package com.testproject.citytemperature.service.impl;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.testproject.citytemperature.dao.model.Country;
import com.testproject.citytemperature.dao.repository.CountryRepository;
import com.testproject.citytemperature.service.CountryService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class CountryServiceTest {
    private final String NAME = "Russia";
    private final String NAME_BAD = "nameBad";
    private final String CODE = "RU";
    private final Long ID = 1l;
    private final Country country = Country.builder()
            .id(ID)
            .name(NAME)
            .build();

    @Mock
    CountryRepository repository;

    @InjectMocks
    @Spy
    CountryService service;

    @Test
    public void get_ok() {
        when(repository.findByName(NAME)).thenReturn(country);

        Country country = service.get(NAME);

        assertEquals(this.country, country);
    }

    @Test
    public void get_countryNotFound() {
        when(repository.findByName(NAME)).thenReturn(null);

        assertNull(service.get(NAME));
    }

    @Test
    public void getCode_ok() {
        String code = service.getCode(NAME);

        assertEquals(this.CODE, code);
    }

    @Test
    public void getCode_nameBad() {
        assertNull(service.getCode(NAME_BAD));
    }

    @Test
    public void saveIfAbsentOrReturn_ok() {
        when(service.get(NAME)).thenReturn(country);

        Country country = service.saveIfAbsentOrReturn(NAME);

        assertEquals(this.country, country);
    }

    @Test
    public void saveIfAbsentOrReturn_countryIsNull() {
        when(service.get(NAME)).thenReturn(null);
        when(repository.save(any(Country.class))).thenReturn(country);

        Country country = service.saveIfAbsentOrReturn(NAME);

        assertEquals(this.country, country);
    }
}