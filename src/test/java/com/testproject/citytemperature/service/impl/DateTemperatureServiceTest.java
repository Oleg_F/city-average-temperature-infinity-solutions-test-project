package com.testproject.citytemperature.service.impl;

import static java.util.Collections.emptyList;
import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.when;

import com.testproject.citytemperature.api.dto.DateTemperatureDto;
import com.testproject.citytemperature.dao.model.City;
import com.testproject.citytemperature.dao.model.CityDateTemperature;
import com.testproject.citytemperature.mapper.CityDateTemperatureMapper;
import com.testproject.citytemperature.service.CityDateTemperatureService;
import com.testproject.citytemperature.service.CityService;
import com.testproject.citytemperature.service.DateTemperatureService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.Instant;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
public class DateTemperatureServiceTest {
    private final String COUNTRY_NAME = "Russia";
    private final String CITY_NAME = "Moscow";
    private final Long ID = 1l;
    private final Long CITY_ID = ID;
    private final Long COUNTRY_ID = ID;
    private final Instant DATE_INSTANT = Instant.MIN;
    private final LocalDate DATE_LOCAL_DATE = LocalDate.MIN;
    private final Double TEMPERATURE = Double.valueOf(0);
    private final City city = City.builder()
            .id(CITY_ID)
            .name(CITY_NAME)
            .countryId(COUNTRY_ID)
            .build();
    private final CityDateTemperature cityDateTemperature = CityDateTemperature.builder()
            .id(ID)
            .cityId(CITY_ID)
            .date(DATE_INSTANT)
            .temperature(TEMPERATURE)
            .build();
    private final List<CityDateTemperature> cityDateTemperatureList = Collections.singletonList(cityDateTemperature);
    private final DateTemperatureDto dto = DateTemperatureDto.builder()
            .date(DATE_INSTANT)
            .temperature(TEMPERATURE)
            .build();
    private final List<DateTemperatureDto> dtoList = Collections.singletonList(dto);

    @Mock
    private CityService cityService;
    @Mock
    private CityDateTemperatureService cityDateTemperatureService;
    @Mock
    private CityDateTemperatureMapper cityDateTemperatureMapper;

    @InjectMocks
    private DateTemperatureService service;

    @Test
    public void getWithMaxDate_ok() {
        when(cityService.get(CITY_NAME, COUNTRY_NAME)).thenReturn(city);
        when(cityDateTemperatureService.getWithMaxDate(CITY_ID)).thenReturn(cityDateTemperature);
        when(cityDateTemperatureMapper.toDateTemperatureDto(cityDateTemperature)).thenReturn(dto);

        DateTemperatureDto dto = service.getWithMaxDate(COUNTRY_NAME, CITY_NAME);

        assertEquals(this.dto, dto);
    }

    @Test
    public void getWithMaxDate_cityIsNull() {
        when(cityService.get(CITY_NAME, COUNTRY_NAME)).thenReturn(null);

        assertNull(service.getWithMaxDate(COUNTRY_NAME, CITY_NAME));
    }

    @Test
    public void getWithMaxDate_cityIdIsNull() {
        Long id = city.getId();
        city.setId(null);

        when(cityService.get(CITY_NAME, COUNTRY_NAME)).thenReturn(city);

        assertNull(service.getWithMaxDate(COUNTRY_NAME, CITY_NAME));

        city.setId(id);
    }

    @Test
    public void getWithMaxDate_cityDateTemperatureIsNull() {
        when(cityService.get(CITY_NAME, COUNTRY_NAME)).thenReturn(city);
        when(cityDateTemperatureService.getWithMaxDate(CITY_ID)).thenReturn(null);

        assertNull(service.getWithMaxDate(COUNTRY_NAME, CITY_NAME));
    }

    @Test
    public void getWithMaxDate_cityDateTemperatureMapperReturnNull() {
        when(cityService.get(CITY_NAME, COUNTRY_NAME)).thenReturn(city);
        when(cityDateTemperatureService.getWithMaxDate(CITY_ID)).thenReturn(cityDateTemperature);
        when(cityDateTemperatureMapper.toDateTemperatureDto(cityDateTemperature)).thenReturn(null);

        assertNull(service.getWithMaxDate(COUNTRY_NAME, CITY_NAME));
    }

    @Test
    public void getList_ok() {
        when(cityService.get(CITY_NAME, COUNTRY_NAME)).thenReturn(city);
        when(cityDateTemperatureService.getListOrderedByDate(CITY_ID, DATE_LOCAL_DATE)).thenReturn(cityDateTemperatureList);
        when(cityDateTemperatureMapper.iterableToDateTemperatureDtoList(cityDateTemperatureList)).thenReturn(dtoList);

        List<DateTemperatureDto> dtoList = service.getList(COUNTRY_NAME, CITY_NAME, DATE_LOCAL_DATE);

        assertEquals(this.dtoList, dtoList);
    }

    @Test
    public void getList_cityIsNull() {
        when(cityService.get(CITY_NAME, COUNTRY_NAME)).thenReturn(null);

        assertNull(service.getList(COUNTRY_NAME, CITY_NAME, DATE_LOCAL_DATE));
    }

    @Test
    public void getList_cityIdIsNull() {
        Long cityId = city.getId();
        city.setId(null);

        when(cityService.get(CITY_NAME, COUNTRY_NAME)).thenReturn(city);

        assertNull(service.getList(COUNTRY_NAME, CITY_NAME, DATE_LOCAL_DATE));
        city.setId(cityId);
    }

    @Test
    public void getList_cityDateTemperatureListIsNull() {
        when(cityService.get(CITY_NAME, COUNTRY_NAME)).thenReturn(city);
        when(cityDateTemperatureService.getListOrderedByDate(CITY_ID, DATE_LOCAL_DATE)).thenReturn(null);

        assertNull(service.getList(COUNTRY_NAME, CITY_NAME, DATE_LOCAL_DATE));
    }

    @Test
    public void getList_cityDateTemperatureListIsEmpty() {
        when(cityService.get(CITY_NAME, COUNTRY_NAME)).thenReturn(city);
        when(cityDateTemperatureService.getListOrderedByDate(CITY_ID, DATE_LOCAL_DATE)).thenReturn(emptyList());

        assertNull(service.getList(COUNTRY_NAME, CITY_NAME, DATE_LOCAL_DATE));
    }

    @Test
    public void getList_cityDateTemperatureMapperReturnNull() {
        when(cityService.get(CITY_NAME, COUNTRY_NAME)).thenReturn(city);
        when(cityDateTemperatureService.getListOrderedByDate(CITY_ID, DATE_LOCAL_DATE)).thenReturn(cityDateTemperatureList);
        when(cityDateTemperatureMapper.iterableToDateTemperatureDtoList(cityDateTemperatureList)).thenReturn(null);

        assertNull(service.getList(COUNTRY_NAME, CITY_NAME, DATE_LOCAL_DATE));
    }
}