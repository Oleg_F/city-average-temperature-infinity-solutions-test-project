package com.testproject.citytemperature.service.impl;

import static java.util.Collections.emptyMap;
import static java.util.Collections.emptySet;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.testproject.citytemperature.api.dto.CityTemperatureDto;
import com.testproject.citytemperature.dao.model.City;
import com.testproject.citytemperature.dao.model.CityDateTemperature;
import com.testproject.citytemperature.dao.model.Country;
import com.testproject.citytemperature.dao.repository.CityDateTemperatureRepository;
import com.testproject.citytemperature.mapper.LocalDateMapper;
import com.testproject.citytemperature.service.CityDateTemperatureService;
import com.testproject.citytemperature.service.CityService;
import com.testproject.citytemperature.service.CountryService;
import com.testproject.citytemperature.service.WeatherService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.Instant;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = CityDateTemperatureService.class)
@EnableConfigurationProperties
public class CityDateTemperatureServiceTest {
    private final Long ID = 1L;
    private final Long CITY_ID = ID;
    private final Instant DATE_INSTANT = Instant.parse("2001-01-01T00:00:00.00Z");
    private final Instant DATE_INSTANT_START_DAY = Instant.parse("2001-01-01T00:00:00.00Z");
    private final Instant DATE_INSTANT_END_DAY = Instant.parse("2001-01-01T23:59:59.00Z");
    private final LocalDate DATE_LOCAL_DATE = LocalDate.MIN;
    private final Double TEMPERATURE = Double.MIN_VALUE;
    private final CityDateTemperature model = CityDateTemperature.builder()
            .id(ID)
            .cityId(CITY_ID)
            .date(DATE_INSTANT)
            .temperature(TEMPERATURE)
            .build();
    private final List<CityDateTemperature> modelList = singletonList(model);
    private final String COUNTRY_NAME = "Russia";
    private final String CITY_NAME = "Moscow";
    private final Long COUNTRY_ID = ID;
    private final CityTemperatureDto cityTemperatureDto = CityTemperatureDto.builder()
            .cityName(CITY_NAME)
            .temperature(TEMPERATURE)
            .build();
    private final Map<String, Set<CityTemperatureDto>> cityTemperatureDtoSetByCountryMap = createCityTemperatureDtoSetByCountryMap();
    private final City city = City.builder()
            .id(ID)
            .name(CITY_NAME)
            .countryId(ID)
            .build();

    private final Country country = Country.builder()
            .id(ID)
            .name(COUNTRY_NAME)
            .build();

    @MockBean
    private CityDateTemperatureRepository repository;
    @MockBean
    private LocalDateMapper localDateMapper;
    @MockBean
    private WeatherService weatherService;
    @MockBean
    private CountryService countryService;
    @MockBean
    private CityService cityService;

    @Autowired
    private CityDateTemperatureService service;

    @Before
    public void before() {
        when(localDateMapper.toInstant(DATE_LOCAL_DATE, true)).thenReturn(DATE_INSTANT_START_DAY);
        when(localDateMapper.toInstant(DATE_LOCAL_DATE, false)).thenReturn(DATE_INSTANT_END_DAY);
    }

    @Test
    public void getWithMaxDate_ok() {
        when(repository.findFirstByCityIdOrderByDateDesc(CITY_ID)).thenReturn(model);

        CityDateTemperature model = service.getWithMaxDate(CITY_ID);

        assertEquals(this.model, model);
    }

    @Test
    public void getWithMaxDate_cityIdNotFound() {
        when(repository.findFirstByCityIdOrderByDateDesc(CITY_ID)).thenReturn(null);

        assertNull(service.getWithMaxDate(CITY_ID));
    }

    @Test
    public void getListOrderedByDate_ok() {
        when(repository.findByCityIdAndDateBetweenOrderByDateAsc(CITY_ID, DATE_INSTANT_START_DAY, DATE_INSTANT_END_DAY))
                .thenReturn(modelList);

        List<CityDateTemperature> modelList = service.getListOrderedByDate(CITY_ID, DATE_LOCAL_DATE);

        assertEquals(this.modelList, modelList);
    }

    @Test
    public void getListOrderedByDate_cityIdOrDateNotFound() {
        when(repository.findByCityIdAndDateBetweenOrderByDateAsc(CITY_ID, DATE_INSTANT_START_DAY, DATE_INSTANT_END_DAY))
                .thenReturn(null);

        assertNull(service.getListOrderedByDate(CITY_ID, DATE_LOCAL_DATE));
    }

    @Test
    public void autoSave_ok() throws Exception {
        when(weatherService.getCityTemperatureDtoSetByCountryNameMap(anyMap())).thenReturn(cityTemperatureDtoSetByCountryMap);
        when(countryService.saveIfAbsentOrReturn(COUNTRY_NAME)).thenReturn(country);
        when(cityService.saveIfAbsentOrReturn(CITY_NAME, COUNTRY_ID)).thenReturn(city);

        invokeAutoSaveMethod();

        verify(weatherService).getCityTemperatureDtoSetByCountryNameMap(anyMap());
        verify(countryService).saveIfAbsentOrReturn(COUNTRY_NAME);
        verify(cityService).saveIfAbsentOrReturn(CITY_NAME, COUNTRY_ID);
        verify(repository).save(any(CityDateTemperature.class));
    }

    @Test
    public void autoSave_citiesByCountry_IsNull() throws Exception {
        Map<String, Set<String>> citiesByCountry = service.getCitiesByCountry();
        service.setCitiesByCountry(null);

        try {
            invokeAutoSaveMethod();
        } catch (InvocationTargetException e) {
            service.setCitiesByCountry(citiesByCountry);
            assertTrue(e.getCause() instanceof NullPointerException);
        }
    }

    @Test
    public void autoSave_citiesByCountry_IsEmpty() throws Exception {
        Map<String, Set<String>> citiesByCountry = service.getCitiesByCountry();
        service.setCitiesByCountry(new HashMap<>());

        try {
            invokeAutoSaveMethod();
        } catch (InvocationTargetException e) {
            service.setCitiesByCountry(citiesByCountry);
            assertTrue(e.getCause() instanceof NullPointerException);
        }
    }

    @Test
    public void autoSave_citiesByCountry_citiesAreNull() throws Exception {
        Map<String, Set<String>> citiesByCountry = service.getCitiesByCountry();
        Set<String> citiesSet = citiesByCountry.get(COUNTRY_NAME);
        citiesByCountry.put(COUNTRY_NAME, null);

        try {
            invokeAutoSaveMethod();
        } catch (InvocationTargetException e) {
            citiesByCountry.put(COUNTRY_NAME, citiesSet);
            assertTrue(e.getCause() instanceof NullPointerException);
        }
    }

    @Test
    public void autoSave_citiesByCountry_citiesAreEmpty() throws Exception {
        Map<String, Set<String>> citiesByCountry = service.getCitiesByCountry();
        Set<String> citiesSet = citiesByCountry.get(COUNTRY_NAME);
        citiesByCountry.put(COUNTRY_NAME, emptySet());

        try {
            invokeAutoSaveMethod();
        } catch (InvocationTargetException e) {
            citiesByCountry.put(COUNTRY_NAME, citiesSet);
            assertTrue(e.getCause() instanceof NullPointerException);
        }
    }

    @Test
    public void autoSave_cityTemperatureDtoSetByCountryMap_isNull() throws Exception {
        when(weatherService.getCityTemperatureDtoSetByCountryNameMap(anyMap())).thenReturn(null);

        try {
            invokeAutoSaveMethod();
        } catch (InvocationTargetException e) {
            assertTrue(e.getCause() instanceof RuntimeException);
        }
    }

    @Test
    public void autoSave_cityTemperatureDtoSetByCountryMap_isEmpty() throws Exception {
        when(weatherService.getCityTemperatureDtoSetByCountryNameMap(anyMap())).thenReturn(emptyMap());

        try {
            invokeAutoSaveMethod();
        } catch (InvocationTargetException e) {
            assertTrue(e.getCause() instanceof RuntimeException);
        }
    }

    @Test
    public void autoSave_cityTemperatureDtoSetByCountryMap_AllSetsAreNull() throws Exception {
        Map<String, Set<CityTemperatureDto>> map = new HashMap<>();
        map.put(COUNTRY_NAME, null);
        when(weatherService.getCityTemperatureDtoSetByCountryNameMap(anyMap())).thenReturn(map);

        try {
            invokeAutoSaveMethod();
        } catch (InvocationTargetException e) {
            assertTrue(e.getCause() instanceof RuntimeException);
        }
    }

    @Test
    public void autoSave_cityTemperatureDtoSetByCountryMap_AllSetsAreEmpty() throws Exception {
        Map<String, Set<CityTemperatureDto>> map = new HashMap<>();
        map.put(COUNTRY_NAME, emptySet());
        when(weatherService.getCityTemperatureDtoSetByCountryNameMap(anyMap())).thenReturn(map);

        try {
            invokeAutoSaveMethod();
        } catch (InvocationTargetException e) {
            assertTrue(e.getCause() instanceof RuntimeException);
        }
    }

    @Test
    public void save_CityTemperatureDto_Instant_Long__ok() throws Exception {
        when(cityService.saveIfAbsentOrReturn(CITY_NAME, COUNTRY_ID)).thenReturn(city);

        invoke__Save_CityTemperatureDto_Instant_Long_Method(cityTemperatureDto, DATE_INSTANT, COUNTRY_ID);

        verify(cityService).saveIfAbsentOrReturn(CITY_NAME, COUNTRY_ID);
    }

    @Test
    public void save_CityTemperatureDto_Instant_Long__cityNameIsNull() throws Exception {
        String cityName = cityTemperatureDto.getCityName();
        cityTemperatureDto.setCityName(null);

        try {
            invoke__Save_CityTemperatureDto_Instant_Long_Method(cityTemperatureDto, DATE_INSTANT, COUNTRY_ID);
        } catch (InvocationTargetException e) {
            cityTemperatureDto.setCityName(cityName);
            assertTrue(e.getCause() instanceof NullPointerException);
        }
    }

    @Test
    public void save_CityTemperatureDto_Instant_Long__temperatureIsNull() throws Exception {
        Double temperature = cityTemperatureDto.getTemperature();
        cityTemperatureDto.setTemperature(null);

        try {
            invoke__Save_CityTemperatureDto_Instant_Long_Method(cityTemperatureDto, DATE_INSTANT, COUNTRY_ID);
        } catch (InvocationTargetException e) {
            cityTemperatureDto.setTemperature(temperature);
            assertTrue(e.getCause() instanceof NullPointerException);
        }
    }

    @Test
    public void save_CityTemperatureDto_Instant_Long__cityIsNull() throws Exception {
        when(cityService.saveIfAbsentOrReturn(CITY_NAME, COUNTRY_ID)).thenReturn(null);

        try {
            invoke__Save_CityTemperatureDto_Instant_Long_Method(cityTemperatureDto, DATE_INSTANT, COUNTRY_ID);
        } catch (InvocationTargetException e) {
            assertTrue(e.getCause() instanceof NullPointerException);
        }
    }

    private void invoke__Save_CityTemperatureDto_Instant_Long_Method(CityTemperatureDto cityTemperatureDto,
                                                                     Instant date,
                                                                     Long countryId) throws Exception {
        getMethod("save", CityTemperatureDto.class, Instant.class, Long.class)
                .invoke(service, cityTemperatureDto, date, countryId);
    }

    private void invokeAutoSaveMethod() throws Exception {
        getMethod("autoSave").invoke(service);
    }

    private Method getMethod(String name, Class<?>... parameterTypes) throws NoSuchMethodException {
        Method method = CityDateTemperatureService.class.getDeclaredMethod(name, parameterTypes);
        method.setAccessible(true);

        return method;
    }

    private Map<String, Set<CityTemperatureDto>> createCityTemperatureDtoSetByCountryMap() {
        Map<String, Set<CityTemperatureDto>> map = new HashMap<>();
        map.put(COUNTRY_NAME, new HashSet<>(singletonList(cityTemperatureDto)));

        return map;
    }
}