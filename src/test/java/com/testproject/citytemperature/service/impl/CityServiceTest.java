package com.testproject.citytemperature.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.testproject.citytemperature.dao.model.City;
import com.testproject.citytemperature.dao.model.Country;
import com.testproject.citytemperature.dao.repository.CityRepository;
import com.testproject.citytemperature.service.CityService;
import com.testproject.citytemperature.service.CountryService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class CityServiceTest {
    private final String NAME = "Moscow";
    private final String COUNTRY_NAME = "Russia";
    private final Long ID = 1l;
    private final Long COUNTRY_ID = ID;
    private final Country country = Country.builder()
            .id(COUNTRY_ID)
            .name(COUNTRY_NAME)
            .build();
    private final City city = City.builder()
            .id(ID)
            .name(NAME)
            .countryId(COUNTRY_ID)
            .build();

    @Mock
    private CityRepository repository;
    @Mock
    private CountryService countryService;

    @InjectMocks
    private CityService service;

    @Test
    public void get_ok() {
        when(countryService.get(COUNTRY_NAME)).thenReturn(country);
        when(repository.findByNameAndCountryId(NAME, COUNTRY_ID)).thenReturn(city);

        City city = service.get(NAME, COUNTRY_NAME);
        assertEquals(this.city, city);
    }

    @Test
    public void get_countryIsNull() {
        when(countryService.get(COUNTRY_NAME)).thenReturn(null);

        assertNull(service.get(NAME, COUNTRY_NAME));
    }

    @Test
    public void get__get_Name_CountryId_returnNull() {
        when(countryService.get(COUNTRY_NAME)).thenReturn(country);
        when(repository.findByNameAndCountryId(NAME, COUNTRY_ID)).thenReturn(null);

        assertNull(service.get(NAME, COUNTRY_NAME));
    }

    @Test
    public void saveIfAbsentOrReturn_ok() {
        when(repository.findByNameAndCountryId(NAME, COUNTRY_ID)).thenReturn(city);

        City city = service.saveIfAbsentOrReturn(NAME, COUNTRY_ID);

        assertEquals(this.city, city);
    }

    @Test
    public void saveIfAbsentOrReturn_cityIsNull() {
        when(repository.findByNameAndCountryId(NAME, COUNTRY_ID)).thenReturn(null);
        when(repository.save(any(City.class))).thenReturn(city);

        City city = service.saveIfAbsentOrReturn(NAME, COUNTRY_ID);

        assertEquals(this.city, city);
    }
}