package com.testproject.citytemperature.service.impl;

import static com.testproject.citytemperature.service.impl.WeatherbitServiceTest.API_KEY;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.NO_CONTENT;

import com.testproject.citytemperature.api.dto.CityTemperatureDto;
import com.testproject.citytemperature.api.dto.CurrentObsDto;
import com.testproject.citytemperature.api.dto.CurrentObsGroupDto;
import com.testproject.citytemperature.feign.WeatherbitFeinClient;
import com.testproject.citytemperature.mapper.CurrentObsDtoMapper;
import com.testproject.citytemperature.service.WeatherProviderService;
import com.testproject.citytemperature.service.WeatherbitService;
import com.testproject.citytemperature.utils.FeignExceptionUtils;
import feign.FeignException;
import feign.RetryableException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Collections;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WeatherbitService.class)
@TestPropertySource(properties = "app.weather-providers.weatherbit.key=" + API_KEY)
public class WeatherbitServiceTest {
    public static final String API_KEY = "7688731506a9408182b531457325703a";
    public final Double TEMP = Double.MIN_VALUE;
    public final Double TEMPERATURE = TEMP;
    private final String API_KEY_BAD = API_KEY;
    private final String CITY_NAME = "Moscow";
    private final String CITY_NAME_BAD = "CityNameBad";
    private final String COUNTRY_CODE = "RU";
    private final String COUNTRY_CODE_BAD = "CountryCodeBad";
    private final Integer COUNT = 1;
    private final CityTemperatureDto cityTemperatureDto = CityTemperatureDto.builder()
            .cityName(CITY_NAME)
            .temperature(TEMPERATURE)
            .build();
    public final CurrentObsDto currentObsDto = CurrentObsDto.builder()
            .cityName(CITY_NAME)
            .temp(TEMP)
            .build();
    public final CurrentObsGroupDto currentObsGroupDto = CurrentObsGroupDto.builder()
            .count(COUNT)
            .data(Collections.singletonList(currentObsDto))
            .build();
    @MockBean
    private WeatherbitFeinClient feignClient;
    @MockBean
    private CurrentObsDtoMapper currentObsDtoMapper;
    @Autowired
    private WeatherProviderService service;

    @Before
    public void before() {
        when(currentObsDtoMapper.toCityTemperatureDto(currentObsDto)).thenReturn(cityTemperatureDto);
    }

    @Test
    public void getCityTemperatureDto_ok() {
        when(feignClient.getCurrentWeather(COUNTRY_CODE, CITY_NAME, API_KEY))
                .thenReturn(new ResponseEntity<>(currentObsGroupDto, HttpStatus.OK));

        CityTemperatureDto cityTemperatureDto = service.getCityTemperatureDto(COUNTRY_CODE, CITY_NAME);
        assertEquals(this.cityTemperatureDto, cityTemperatureDto);
    }

    @Test
    public void getCityTemperatureDto_countryCodeBad() {
        when(feignClient.getCurrentWeather(COUNTRY_CODE_BAD, CITY_NAME, API_KEY))
                .thenReturn(new ResponseEntity<>(currentObsGroupDto, HttpStatus.OK));

        CityTemperatureDto cityTemperatureDto = service.getCityTemperatureDto(COUNTRY_CODE_BAD, CITY_NAME);
        assertEquals(this.cityTemperatureDto, cityTemperatureDto);
    }

    @Test(expected = RuntimeException.class)
    public void getCityTemperatureDto_cityNameBad() {
        when(feignClient.getCurrentWeather(COUNTRY_CODE, CITY_NAME_BAD, API_KEY))
                .thenReturn(new ResponseEntity<>(NO_CONTENT));

        service.getCityTemperatureDto(COUNTRY_CODE, CITY_NAME_BAD);
    }

    @Test
    public void getCityTemperatureDto_apiKeyBad() {
        String responseBody = "{\"error\":\"API key not valid, or not yet activated.\"}";
        FeignException feignException = FeignExceptionUtils.create(responseBody, FORBIDDEN.value());

        when(feignClient.getCurrentWeather(COUNTRY_CODE, CITY_NAME, API_KEY_BAD))
                .thenThrow(feignException);

        try {
            service.getCityTemperatureDto(COUNTRY_CODE, CITY_NAME);
        } catch (FeignException e) {
            assertEquals(FORBIDDEN.value(), e.status());
            assertEquals(responseBody, e.contentUTF8());
        }
    }

    @Test(expected = RetryableException.class)
    public void getCityTemperatureDto_resourceIsUnavailable() {
        when(feignClient.getCurrentWeather(COUNTRY_CODE, CITY_NAME, API_KEY)).thenThrow(RetryableException.class);

        service.getCityTemperatureDto(COUNTRY_CODE, CITY_NAME);
    }
}