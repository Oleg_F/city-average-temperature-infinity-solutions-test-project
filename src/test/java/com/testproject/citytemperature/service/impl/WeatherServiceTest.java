package com.testproject.citytemperature.service.impl;

import static java.util.Collections.emptyMap;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.testproject.citytemperature.api.dto.CityTemperatureDto;
import com.testproject.citytemperature.service.CountryService;
import com.testproject.citytemperature.service.WeatherProviderService;
import com.testproject.citytemperature.service.WeatherService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

@RunWith(SpringJUnit4ClassRunner.class)
public class WeatherServiceTest {
    private final String COUNTRY_NAME = "Russia";
    private final String COUNTRY_CODE = "RU";
    private final String CITY_NAME = "Moscow";
    private final String CITY_NAME2 = "Saratov";
    private final Double TEMPERATURE = Double.valueOf(0);
    private final Map<String, Set<String>> cityNameSetByCountryNameMap = createCityNameSetByCountryNameMap();
    private final CityTemperatureDto cityTemperatureDto = CityTemperatureDto.builder()
            .cityName(CITY_NAME)
            .temperature(TEMPERATURE)
            .build();

    @Mock
    private WeatherProviderService weatherProviderService;

    @Mock
    private CountryService countryService;
    @Mock
    private List<WeatherProviderService> weatherProviderServiceList;

    @InjectMocks
    private WeatherService service;

    @Test
    public void getCityTemperatureDtoByCountryMap_ok() {
        when(countryService.getCode(COUNTRY_NAME)).thenReturn(COUNTRY_CODE);
        when(weatherProviderServiceList.stream()).then(answer -> Stream.of(weatherProviderService));
        when(weatherProviderService.getCityTemperatureDto(anyString(), anyString())).thenReturn(cityTemperatureDto);

        Map<String, Set<CityTemperatureDto>> cityTemperatureDtoSetByCountryNameMap =
                service.getCityTemperatureDtoSetByCountryNameMap(cityNameSetByCountryNameMap);

        CityTemperatureDto cityTemperatureDto = cityTemperatureDtoSetByCountryNameMap.get(COUNTRY_NAME).stream()
                .findFirst()
                .get();

        assertEquals(this.cityTemperatureDto, cityTemperatureDto);
    }

    @Test(expected = RuntimeException.class)
    public void getCityTemperatureDtoByCountryMap_cityNameSetByCountryNameMapIsNull() {
        service.getCityTemperatureDtoSetByCountryNameMap(null);
    }

    @Test(expected = RuntimeException.class)
    public void getCityTemperatureDtoByCountryMap_cityNameSetByCountryNameMapIsEmpty() {
        service.getCityTemperatureDtoSetByCountryNameMap(emptyMap());
    }

    @Test
    public void getCityTemperatureDtoByCountryMap_countryCodeIsNull() {
        when(countryService.getCode(COUNTRY_NAME)).thenReturn(null);

        service.getCityTemperatureDtoSetByCountryNameMap(cityNameSetByCountryNameMap);

        verify(countryService).getCode(COUNTRY_NAME);
    }

    @Test
    public void getCityTemperatureDtoByCountryMap_cityTemperatureDtoIsNull() {
        when(countryService.getCode(COUNTRY_NAME)).thenReturn(COUNTRY_CODE);
        when(weatherProviderServiceList.stream()).then(answer -> Stream.of(weatherProviderService));
        when(weatherProviderService.getCityTemperatureDto(anyString(), anyString())).thenReturn(null);

        Map<String, Set<CityTemperatureDto>> cityTemperatureDtoSetByCountryNameMap =
                service.getCityTemperatureDtoSetByCountryNameMap(cityNameSetByCountryNameMap);

        assertTrue(cityTemperatureDtoSetByCountryNameMap.isEmpty());
    }

    private Map<String, Set<String>> createCityNameSetByCountryNameMap() {
        Map<String, Set<String>> map = new HashMap<>();
        map.put(COUNTRY_NAME, new HashSet<>(Arrays.asList(CITY_NAME, CITY_NAME2)));
        return map;
    }
}