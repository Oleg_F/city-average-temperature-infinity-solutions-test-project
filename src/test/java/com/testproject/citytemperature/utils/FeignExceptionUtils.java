package com.testproject.citytemperature.utils;

import static java.util.Collections.emptyMap;
import static org.apache.commons.lang3.StringUtils.EMPTY;

import feign.FeignException;
import feign.Request;
import feign.RequestTemplate;
import lombok.NonNull;

import java.nio.charset.StandardCharsets;

public abstract class FeignExceptionUtils {
    public static FeignException create(@NonNull String responseBody, int responseStatus) {
        byte[] responseBodyByteArray = responseBody.getBytes(StandardCharsets.UTF_8);
        Request request = Request.create(Request.HttpMethod.GET, EMPTY, emptyMap(), null, new RequestTemplate());

        return new FeignException.FeignClientException(responseStatus, null, request, responseBodyByteArray, null);
    }
}