package com.testproject.citytemperature.utils;


import static org.junit.Assert.assertEquals;

import feign.FeignException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class FeignExceptionUtilsTest {
    private final String RESPONSE_BODY = "response body";
    private final int RESPONSE_STATUS = HttpStatus.OK.value();

    @Test
    public void create_ok() {
        FeignException feignException = FeignExceptionUtils.create(RESPONSE_BODY, RESPONSE_STATUS);

        assertEquals(RESPONSE_BODY, new String(feignException.responseBody().get().array()));
        assertEquals(RESPONSE_STATUS, feignException.status());
    }
}